----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/02/2023 03:18:27 PM
-- Design Name: 
-- Module Name: HSSL_reader - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-------------------------------------------------------
--! @brief Entity definition for the HSSL_reader module.
--!
--! This entity defines the HSSL_reader module with generic parameters and ports.
--! @generic
--!     @param GAP_BITS   Number of gap bits to wait for after reading all data (default: 10).
--!     @param DATA_WIDTH Width of the data input and output (default: 48).
--! @port
--!     @name 
-------------------------------------------------------
entity HSSL_reader is
    generic (
        GAP_BITS   : integer := 10;
        DATA_WIDTH : integer := 48
    );
    Port ( 
        clk_in     : in STD_LOGIC; -- Clock input signal.
        data_in    : in STD_LOGIC; -- Serial data input (1 bit at a time).
        result_out : out std_logic_vector(DATA_WIDTH-1 downto 0); -- Output the 48-bit data
        valid_out  : inout STD_LOGIC -- Indicates when reading is complete
    );
end entity HSSL_reader;

--!
--! @brief Behavioral architecture of the HSSL_reader module.
--!
--! This architecture defines the behavior of the HSSL_reader module.
--!
architecture Behavioral of HSSL_reader is
  signal bit_count  : natural := 0;
  signal gap_bit_count  : natural := 0;
  signal buffer_out : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
begin
  process (clk_in)
  begin
    if rising_edge(clk_in) then
      if bit_count < DATA_WIDTH then
        buffer_out(bit_count) <= data_in;
        bit_count <= bit_count + 1;
        valid_out <= '0';
      else
        -- All 48 bits have been; read wait GAP_BITS
        if gap_bit_count < GAP_BITS-1 then
            gap_bit_count <= gap_bit_count + 1;
        else
            valid_out <= '1';
        end if;
      end if;
    end if;
  end process;
  
  -- Output the 48-bit result when valid_out
  result_out <= buffer_out when valid_out = '1' else (others => '0');

end Behavioral;
