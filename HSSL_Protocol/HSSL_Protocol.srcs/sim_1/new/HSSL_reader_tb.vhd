----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/02/2023 03:43:58 PM
-- Design Name: 
-- Module Name: HSSL_reader_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity HSSL_reader_tb is
end entity HSSL_reader_tb;

architecture testbench of HSSL_reader_tb is
  signal clk         : std_logic := '0';  -- Clock signal
  signal data_in     : std_logic := '0';  -- Serial data input
  signal result_out  : std_logic_vector(47 downto 0);  -- Output the 48-bit data
  signal valid_out   : std_logic := '0';  -- Indicates when reading is complete
  -- Stop simulation signal
  signal stop_simul : boolean := false;
begin
  -- Instantiate the HSSL_reader module
  uut: entity work.HSSL_reader
    generic map (
      GAP_BITS   => 10,      -- Set your desired GAP_BITS value
      DATA_WIDTH => 48       -- Set your desired DATA_WIDTH value
    )
    port map (
      clk_in     => clk,
      data_in    => data_in,
      result_out => result_out,
      valid_out  => valid_out
    );

  -- Clock generation process
  process
  begin
    clk <= not clk;
    wait for 20 ns;  -- Adjust the clock period as needed (25MHz in this case)
  end process;

  -- Stimulus process
  process
  begin
  
    -- Initialize data_in and valid_out
    data_in <= '0';

    -- Apply a sequence of data values
    for i in 0 to 23 loop
      data_in <= '1';
      wait for 40 ns;
      data_in <= '0';
      wait for 40 ns;
    end loop;

    -- Wait for valid_out to indicate completion
    wait until valid_out = '1';

    -- Stop the simulation
    wait for 1000 ns;
    report "Simulation finished." severity note;
    wait;

    
  end process;


end architecture testbench;

