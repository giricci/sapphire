library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity tb_spi_dac_control is
end entity tb_spi_dac_control;

architecture testbench of tb_spi_dac_control is
  constant ClockFrequency : integer := 10e6; -- 10 MHz
  constant ClockPeriod    : time    := 1000 ms / ClockFrequency;

  -- Constants
  constant SPI_CLK_DIVIDER_TEST : integer := 4;
  constant axis_DATA_WIDTH_TEST : integer := 18;
  
  -- Signals
  signal clk_i_tb : STD_LOGIC := '0';
  signal resetn_i_tb : STD_LOGIC := '1';  -- Assuming active-low reset
  signal data_i_tb : STD_LOGIC_VECTOR(18-1 downto 0) := "011010101010101001";  -- Sample data
  signal sdo_i_tb : STD_LOGIC := '0';  -- Sample slave data input
  signal busyn_i_tb : STD_LOGIC;
  signal sck_o_tb : STD_LOGIC;
  signal sdi_o_tb : STD_LOGIC;
  signal syncn_o_tb : STD_LOGIC;
  signal ldacn_o_tb : STD_LOGIC;
  signal clrn_o_tb  : STD_LOGIC;
  signal dac_resetn_o_tb : STD_LOGIC;

begin
  -- Connect the DUT
  dut : entity work.AD5781(Behavioral)
    generic map (
      SPI_CLK_DIVIDER => SPI_CLK_DIVIDER_TEST,
      axis_DATA_WIDTH => axis_DATA_WIDTH_TEST
    )
    port map (
      clk_i        => clk_i_tb,
      resetn_i     => resetn_i_tb,
      data_i       => data_i_tb,
      sdo_i        => sdo_i_tb,
      ldacn_o      => ldacn_o_tb,
      syncn_o      => syncn_o_tb,
      clrn_o       => clrn_o_tb,
      dac_resetn_o => dac_resetn_o_tb,
      sdi_o        => sdi_o_tb
    );

  -- Clock generation process
  clk_i_tb <= not clk_i_tb after ClockPeriod / 2;

  -- Simulation process
  process
  begin
    -- Initialize resetn_i_tb
    resetn_i_tb <= '0';
    wait for 10 ns;
    resetn_i_tb <= '1';
    busyn_i_tb  <= '1';
    wait for 10000 ns;
    resetn_i_tb <= '0';
    busyn_i_tb  <= '1';
    wait for 50 ns;

    -- Add additional simulation scenarios as needed

    -- Finish simulation
    wait for 10000 ns; -- Allow simulation to continue
    report "Simulation finished." severity note;
    wait;
  end process;

end architecture testbench;
