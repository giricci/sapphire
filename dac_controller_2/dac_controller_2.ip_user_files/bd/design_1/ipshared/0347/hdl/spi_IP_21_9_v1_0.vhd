library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi_IP_21_9_v1_0 is
	generic (
		-- Users to add parameters here
        SPI_CLK_DIVIDER : integer := 4; -- Adjust this value for desired SPI clock speed.
        axis_DATA_WIDTH : integer := 18; -- Adjust this value for desired axis data width.
		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 4
	);
	port (
		-- Users to add ports here
        clk_i        : in STD_LOGIC;
        resetn_i     : in STD_LOGIC;
        sdo_i        : in STD_LOGIC;
        --sck_o        : out STD_LOGIC;
        ldacn_o      : out STD_LOGIC;
        syncn_o      : out STD_LOGIC;
        clrn_o       : out STD_LOGIC;
        dac_resetn_o : out STD_LOGIC;
        sdi_o        : out STD_LOGIC;
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end spi_IP_21_9_v1_0;

architecture arch_imp of spi_IP_21_9_v1_0 is

	-- component declaration
	component spi_IP_21_9_v1_0_S00_AXI is
		generic (
		SPI_CLK_DIVIDER : integer := 4; -- Adjust this value for desired SPI clock speed.
        axis_DATA_WIDTH : integer := 18; -- Adjust this value for desired axis data width.
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 4
		);
		port (
		clk_i        : in STD_LOGIC;
        resetn_i     : in STD_LOGIC;
        sdo_i        : in STD_LOGIC;
        --sck_o        : out STD_LOGIC;
        ldacn_o      : out STD_LOGIC;
        syncn_o      : out STD_LOGIC;
        clrn_o       : out STD_LOGIC;
        dac_resetn_o : out STD_LOGIC;
        sdi_o        : out STD_LOGIC;
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component spi_IP_21_9_v1_0_S00_AXI;

begin

-- Instantiation of Axi Bus Interface S00_AXI
spi_IP_21_9_v1_0_S00_AXI_inst : spi_IP_21_9_v1_0_S00_AXI
	generic map (
	    SPI_CLK_DIVIDER => SPI_CLK_DIVIDER,
        axis_DATA_WIDTH => axis_DATA_WIDTH,
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
	    clk_i        => clk_i,
        resetn_i     => resetn_i,
        sdo_i        => sdo_i,
        --sck_o        => sck_o,
        ldacn_o      => ldacn_o,
        syncn_o      => syncn_o,
        clrn_o       => clrn_o,
        dac_resetn_o => dac_resetn_o,
        sdi_o        => sdi_o,
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
