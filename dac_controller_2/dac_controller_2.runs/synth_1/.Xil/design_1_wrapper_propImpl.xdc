set_property SRC_FILE_INFO {cfile:c:/Users/giricci/cernbox/WINDOWS/Desktop/dac_controller_2/dac_controller_2.gen/sources_1/bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.xdc rfile:../../../dac_controller_2.gen/sources_1/bd/design_1/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.xdc id:1 order:EARLY scoped_inst:design_1_i/clk_wiz_0/inst} [current_design]
set_property SRC_FILE_INFO {cfile:C:/Users/giricci/cernbox/WINDOWS/Desktop/dac_controller_2/dac_controller_2.srcs/constrs_1/new/constr.xdc rfile:../../../dac_controller_2.srcs/constrs_1/new/constr.xdc id:2} [current_design]
current_instance design_1_i/clk_wiz_0/inst
set_property src_info {type:SCOPED_XDC file:1 line:57 export:INPUT save:INPUT read:READ} [current_design]
set_input_jitter [get_clocks -of_objects [get_ports clk_in1]] 0.100
current_instance
set_property src_info {type:XDC file:2 line:2 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A20 IOSTANDARD LVCMOS33} [get_ports sdo_i_0]
set_property src_info {type:XDC file:2 line:4 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN A21 IOSTANDARD LVCMOS33} [get_ports ldacn_o_0]
set_property src_info {type:XDC file:2 line:5 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN B21 IOSTANDARD LVCMOS33} [get_ports syncn_o_0]
set_property src_info {type:XDC file:2 line:6 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN C21 IOSTANDARD LVCMOS33} [get_ports clrn_o_0]
set_property src_info {type:XDC file:2 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN C22 IOSTANDARD LVCMOS33} [get_ports dac_resetn_o_0]
set_property src_info {type:XDC file:2 line:8 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict {PACKAGE_PIN D21 IOSTANDARD LVCMOS33} [get_ports sdi_o_0]
