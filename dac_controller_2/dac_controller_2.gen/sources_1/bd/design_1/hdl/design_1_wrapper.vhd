--Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
--Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2023.1 (win64) Build 3865809 Sun May  7 15:05:29 MDT 2023
--Date        : Thu Sep 21 12:15:16 2023
--Host        : pcbe17101 running 64-bit major release  (build 9200)
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    clrn_o_0 : out STD_LOGIC;
    dac_resetn_o_0 : out STD_LOGIC;
    ldacn_o_0 : out STD_LOGIC;
    sdi_o_0 : out STD_LOGIC;
    sdo_i_0 : in STD_LOGIC;
    syncn_o_0 : out STD_LOGIC
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    syncn_o_0 : out STD_LOGIC;
    clrn_o_0 : out STD_LOGIC;
    sdo_i_0 : in STD_LOGIC;
    ldacn_o_0 : out STD_LOGIC;
    dac_resetn_o_0 : out STD_LOGIC;
    sdi_o_0 : out STD_LOGIC
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      clrn_o_0 => clrn_o_0,
      dac_resetn_o_0 => dac_resetn_o_0,
      ldacn_o_0 => ldacn_o_0,
      sdi_o_0 => sdi_o_0,
      sdo_i_0 => sdo_i_0,
      syncn_o_0 => syncn_o_0
    );
end STRUCTURE;
